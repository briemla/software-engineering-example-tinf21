package de.dhbw.ase;

import java.time.Duration;
import java.util.Collections;
import java.util.List;

public class Konfiguration {
	private final int anzahlPersonen;
	private final List<AktivitätsArt> aktivitätenArten;
	private final int anzahlAktivitäten;
	private final Duration aktivitätenDauer;
	private final List<Fortbewegungsmittel> fortbewegungsmittel;

	public Konfiguration(int anzahlPersonen, List<AktivitätsArt> aktivitätenArten, int anzahlAktivitäten,
			Duration aktivitätenDauer, List<Fortbewegungsmittel> fortbewegungsmittel) {
		super();
		this.anzahlPersonen = anzahlPersonen;
		this.aktivitätenArten = Collections.unmodifiableList(aktivitätenArten);
		this.anzahlAktivitäten = anzahlAktivitäten;
		this.aktivitätenDauer = aktivitätenDauer;
		this.fortbewegungsmittel = fortbewegungsmittel;
	}

	public int anzahlPersonen() {
		return this.anzahlPersonen;
	}

	public List<AktivitätsArt> aktivitätenArten() {
		return this.aktivitätenArten;
	}

	public int anzahlAktivitäten() {
		return this.anzahlAktivitäten;
	}

	public Duration aktivitätenDauer() {
		return this.aktivitätenDauer;
	}

	public List<Fortbewegungsmittel> fortbewegungsmittel() {
		return Collections.unmodifiableList(this.fortbewegungsmittel);
	}
}

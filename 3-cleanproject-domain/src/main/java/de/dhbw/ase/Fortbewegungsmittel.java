package de.dhbw.ase;

public enum Fortbewegungsmittel {
	Pkw, Fußgänger, Radfahrer, ÖV, Taxi, Sharing, Flugzeug, Schiff
}

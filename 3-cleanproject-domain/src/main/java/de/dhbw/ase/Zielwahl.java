package de.dhbw.ase;

import java.time.LocalDateTime;

public interface Zielwahl {

	Ort wähleNächstesZiel(PersonId person, Ort aktuellenOrt, AktivitätsArt nächsteAktivitätsart,
			LocalDateTime aktuelleZeit);

}

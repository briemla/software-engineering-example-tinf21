package de.dhbw.ase;

import java.time.LocalDateTime;
import java.util.List;

public interface Verkehrsmittelwahl {

	Fortbewegungsmittel wähleNächstenVerkehrsmittel(PersonId id, List<Fortbewegungsmittel> fortbewegungsmittel,
			Ort aktuellerOrt, Ort nächstenOrt, LocalDateTime aktuelleZeit);

}

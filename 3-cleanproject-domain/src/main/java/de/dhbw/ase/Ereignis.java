package de.dhbw.ase;

import java.time.LocalDateTime;

public class Ereignis {
	private final PersonId person;
	private final LocalDateTime zeitpunkt;
	private final EreignisArt art;

	public Ereignis(PersonId person, LocalDateTime zeitpunkt, EreignisArt art) {
		super();
		this.person = person;
		this.zeitpunkt = zeitpunkt;
		this.art = art;
	}

	public PersonId person() {
		return this.person;
	}

	public LocalDateTime zeitpunkt() {
		return this.zeitpunkt;
	}

	public EreignisArt aktivitätsArt() {
		return this.art;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((art == null) ? 0 : art.hashCode());
		result = prime * result + ((person == null) ? 0 : person.hashCode());
		result = prime * result + ((zeitpunkt == null) ? 0 : zeitpunkt.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Ereignis other = (Ereignis) obj;
		if (art != other.art)
			return false;
		if (person == null) {
			if (other.person != null)
				return false;
		} else if (!person.equals(other.person))
			return false;
		if (zeitpunkt == null) {
			if (other.zeitpunkt != null)
				return false;
		} else if (!zeitpunkt.equals(other.zeitpunkt))
			return false;
		return true;
	}

}

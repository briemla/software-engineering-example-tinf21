package de.dhbw.ase;

import java.time.LocalDateTime;

public class Weg {

	private final Fortbewegungsmittel fortbewegungsmittel;
	private final Ort start;
	private final Ort ziel;
	private final LocalDateTime startzeit;
	private final LocalDateTime ankunftszeit;

	public Weg(Fortbewegungsmittel fortbewegungsmittel, Ort start, Ort ziel, LocalDateTime startzeit,
			LocalDateTime ankunftszeit) {
		super();
		this.fortbewegungsmittel = fortbewegungsmittel;
		this.start = start;
		this.ziel = ziel;
		this.startzeit = startzeit;
		this.ankunftszeit = ankunftszeit;
	}

	public LocalDateTime ankunftszeit() {
		return this.ankunftszeit;
	}

	public Ort ziel() {
		return this.ziel;
	}

}

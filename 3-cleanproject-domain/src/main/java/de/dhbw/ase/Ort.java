package de.dhbw.ase;

import java.util.Collections;
import java.util.List;

public class Ort {

	private final Adresse adresse;
	private final Beliebtheit beliebtheit;
	private final List<AktivitätsArt> aktivitätsarten;

	public Ort(Adresse adresse, Beliebtheit beliebtheit, List<AktivitätsArt> aktivitätsarten) {
		super();
		this.adresse = adresse;
		this.beliebtheit = beliebtheit;
		this.aktivitätsarten = aktivitätsarten;
	}

	public List<AktivitätsArt> aktivitätsArt() {
		return Collections.unmodifiableList(this.aktivitätsarten);
	}

}

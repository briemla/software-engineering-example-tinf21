package de.dhbw.ase;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public class Person {

	private final PersonId id;
	private final List<Aktivität> aktivitätenPlan;
	private final List<Fortbewegungsmittel> fortbewegungsmittel;
	private final List<Weg> wege;
	private Optional<Weg> aktuellerWeg;

	public Person(PersonId id, List<Aktivität> aktivitätenPlan, List<Fortbewegungsmittel> fortbewegungsmittel,
			List<Weg> wege) {
		super();
		this.id = id;
		this.aktivitätenPlan = aktivitätenPlan;
		this.fortbewegungsmittel = fortbewegungsmittel;
		this.wege = wege;
	}

	public Ereignis initialisiere() {
		LocalDateTime startzeitpunkt = this.aktivitätenPlan.get(0).startzeit();
		EreignisArt aktivitätsArt = EreignisArt.aktivität_beginnt;
		return new Ereignis(this.id, startzeitpunkt, aktivitätsArt);
	}

	public Optional<Ereignis> macheNächstenSchritt(Ereignis aktuellesEreignis, Zielwahl zielwahl,
			Verkehrsmittelwahl verkehrsmittelwahl, Routenwahl routenwahl) {
		if (this.führtAktivitätAus(aktuellesEreignis)) {
			return this.planeNächstenWeg(zielwahl, aktuellesEreignis, verkehrsmittelwahl, routenwahl);
		}
		return this.starteNächsteAktivität();
	}

	private boolean führtAktivitätAus(Ereignis aktuellesEreignis) {
		return EreignisArt.aktivität_endet == aktuellesEreignis.aktivitätsArt();
	}

	private Optional<Ereignis> planeNächstenWeg(Zielwahl zielwahl, Ereignis aktuellesEreignis,
			Verkehrsmittelwahl verkehrsmittelwahl, Routenwahl routenwahl) {
		this.findeNächsteAktivität().ifPresent(Aktivität::beende);
		Optional<Ort> nächsterOrt = this.bestimmeOrtNächsterAktivität(zielwahl, aktuellesEreignis);
		if (nächsterOrt.isEmpty()) {
			return Optional.empty();
		}
		Fortbewegungsmittel fortbewegungsmittel = this.bestimmeFortbewegungsmittelNächsterWeg(nächsterOrt.get(),
				aktuellesEreignis.zeitpunkt(), verkehrsmittelwahl);
		Weg geplanterWeg = this.bestimmeWeg(nächsterOrt.get(), fortbewegungsmittel, aktuellesEreignis.zeitpunkt(),
				routenwahl);
		aktuellerWeg = Optional.of(geplanterWeg);
		return Optional.of(new Ereignis(this.id, geplanterWeg.ankunftszeit(), EreignisArt.weg_endet));
	}

	private Optional<Ort> bestimmeOrtNächsterAktivität(Zielwahl zielwahl, Ereignis aktuelleEreignis) {
		Ort aktuellerOrt = wege.get(wege.size() - 1).ziel();
		LocalDateTime aktuelleZeit = aktuelleEreignis.zeitpunkt();
		Optional<AktivitätsArt> nächsteAktivitätsart = findeNächsteAktivitätsart();
		return nächsteAktivitätsart.map(art -> zielwahl.wähleNächstesZiel(this.id, aktuellerOrt, art, aktuelleZeit));
	}

	private Optional<AktivitätsArt> findeNächsteAktivitätsart() {
		for (Aktivität aktivität : this.aktivitätenPlan) {
			if (!aktivität.beendet()) {
				return Optional.of(aktivität.art());
			}
		}
		return Optional.empty();
	}

	private Fortbewegungsmittel bestimmeFortbewegungsmittelNächsterWeg(Ort nächstenOrt, LocalDateTime aktuelleZeit,
			Verkehrsmittelwahl verkehrsmittelwahl) {
		Ort aktuellerOrt = wege.get(wege.size() - 1).ziel();
		return verkehrsmittelwahl.wähleNächstenVerkehrsmittel(this.id, this.fortbewegungsmittel, aktuellerOrt,
				nächstenOrt, aktuelleZeit);
	}

	private Weg bestimmeWeg(Ort nächsterOrt, Fortbewegungsmittel fortbewegungsmittel, LocalDateTime aktuelleZeit,
			Routenwahl routenwahl) {
		Ort aktuellerOrt = wege.get(wege.size() - 1).ziel();
		return routenwahl.wähleNächsteRoute(this.id, aktuellerOrt, nächsterOrt, fortbewegungsmittel, aktuelleZeit);
	}

	private Optional<Ereignis> starteNächsteAktivität() {
		this.aktuellerWeg.map(this.wege::add);
		this.aktuellerWeg = Optional.empty();
		Optional<Aktivität> nächsteAktivität = this.findeNächsteAktivität();
		return nächsteAktivität
				.map(aktivität -> new Ereignis(this.id, aktivität.endzeit(), EreignisArt.aktivität_endet));
	}

	private Optional<Aktivität> findeNächsteAktivität() {
		for (Aktivität aktivität : this.aktivitätenPlan) {
			if (!aktivität.beendet()) {
				return Optional.of(aktivität);
			}
		}
		return Optional.empty();
	}

}
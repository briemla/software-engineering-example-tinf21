package de.dhbw.ase;

import java.time.Duration;
import java.util.List;

public interface BevölkerungsGenerator {

	Bevölkerung generiere(int anzahlPersonen, List<AktivitätsArt> aktivitätenArten, int anzahlAktivitäten,
			Duration aktivitätenDauer, List<Fortbewegungsmittel> fortbewegungsmittel);

}
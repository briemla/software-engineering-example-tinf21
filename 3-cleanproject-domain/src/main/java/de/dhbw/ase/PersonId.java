package de.dhbw.ase;

public class PersonId {

	private static int nextId = 0;

	private final int id;

	private PersonId(int id) {
		super();
		this.id = id;
	}

	public static final PersonId createNextId() {
		return new PersonId(nextId++);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PersonId other = (PersonId) obj;
		if (id != other.id)
			return false;
		return true;
	}

}

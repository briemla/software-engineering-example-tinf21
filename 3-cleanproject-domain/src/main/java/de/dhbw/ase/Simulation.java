package de.dhbw.ase;

import java.util.Optional;
import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;

public class Simulation {

	private final BevölkerungsGenerator bevölkerungsgenerator;
	private final Konfiguration konfiguration;
	private final Zielwahl zielwahl;
	private final Verkehrsmittelwahl verkehrsmittelwahl;
	private final Routenwahl routenwahl;

	public Simulation(BevölkerungsGenerator bevölkerungsgenerator, Konfiguration konfiguration, Zielwahl zielwahl,
			Verkehrsmittelwahl verkehrsmittelwahl, Routenwahl routenwahl) {
		super();
		this.bevölkerungsgenerator = bevölkerungsgenerator;
		this.konfiguration = konfiguration;
		this.zielwahl = zielwahl;
		this.verkehrsmittelwahl = verkehrsmittelwahl;
		this.routenwahl = routenwahl;
	}

	public void run() {
		Queue<Ereignis> ereignisse = new LinkedBlockingQueue<Ereignis>();
		Bevölkerung bevölkerung = initialisiere(ereignisse);
		simuliere(ereignisse, bevölkerung);
	}

	private Bevölkerung initialisiere(Queue<Ereignis> ereignisse) {
		Bevölkerung bevölkerung = this.bevölkerungsgenerator.generiere(konfiguration.anzahlPersonen(),
				konfiguration.aktivitätenArten(), konfiguration.anzahlAktivitäten(), konfiguration.aktivitätenDauer(),
				konfiguration.fortbewegungsmittel());
		for (Person person : bevölkerung.personen()) {
			Ereignis nächstesEreignis = person.initialisiere();
			ereignisse.add(nächstesEreignis);
		}
		return bevölkerung;
	}

	private void simuliere(Queue<Ereignis> ereignisse, Bevölkerung bevölkerung) {
		while (!ereignisse.isEmpty()) {
			Ereignis aktuellesEreignis = ereignisse.poll();
			Person person = bevölkerung.suchePerson(aktuellesEreignis.person());
			Optional<Ereignis> nächstesEreignis = person.macheNächstenSchritt(aktuellesEreignis, zielwahl,
					verkehrsmittelwahl, routenwahl);
			nächstesEreignis.map(ereignisse::add);
		}
	}
}

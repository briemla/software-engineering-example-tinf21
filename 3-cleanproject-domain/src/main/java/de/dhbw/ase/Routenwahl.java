package de.dhbw.ase;

import java.time.LocalDateTime;

public interface Routenwahl {

	Weg wähleNächsteRoute(PersonId id, Ort aktuellerOrt, Ort nächsterOrt, Fortbewegungsmittel fortbewegungsmittel,
			LocalDateTime aktuelleZeit);

}

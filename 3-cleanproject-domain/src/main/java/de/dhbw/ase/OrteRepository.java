package de.dhbw.ase;

import java.util.List;

public interface OrteRepository {

	List<Ort> findeOrteFür(AktivitätsArt aktivitätsArt);

}

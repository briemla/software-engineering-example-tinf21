package de.dhbw.ase;

public enum AktivitätsArt {
	Arbeit, Schule, Lebensmitteleinkauf, Veranstaltungen, PrivateBesuche, Freizeit, Arztbesuche, Erledigungen, Urlaub
}

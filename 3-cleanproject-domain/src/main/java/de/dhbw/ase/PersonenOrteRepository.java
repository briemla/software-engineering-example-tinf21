package de.dhbw.ase;

import java.util.List;

public interface PersonenOrteRepository {

	List<Ort> findeOrteFür(PersonId person, AktivitätsArt aktivitätsArt);
}

package de.dhbw.ase;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Random;

public class FlexibleZielwahl implements Zielwahl {

	private final OrteRepository orteRepository;

	public FlexibleZielwahl(OrteRepository orteRepository) {
		super();
		this.orteRepository = orteRepository;
	}

	@Override
	public Ort wähleNächstesZiel(PersonId person, Ort aktuellenOrt, AktivitätsArt nächsteAktivitätsart,
			LocalDateTime aktuelleZeit) {
		List<Ort> möglicheOrte = this.orteRepository.findeOrteFür(nächsteAktivitätsart);
		Random random = new Random();
		int nächstesElement = random.nextInt(möglicheOrte.size());
		return möglicheOrte.get(nächstesElement);
	}

}

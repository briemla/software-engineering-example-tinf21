package de.dhbw.ase;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Random;

public class StatischeZielwahl implements Zielwahl {

	private final PersonenOrteRepository orteRepository;

	public StatischeZielwahl(PersonenOrteRepository orteRepository) {
		super();
		this.orteRepository = orteRepository;
	}

	@Override
	public Ort wähleNächstesZiel(PersonId person, Ort aktuellenOrt, AktivitätsArt nächsteAktivitätsart,
			LocalDateTime aktuelleZeit) {
		List<Ort> möglicheOrte = orteRepository.findeOrteFür(person, nächsteAktivitätsart);
		Random random = new Random();
		int nächstesElement = random.nextInt(möglicheOrte.size());
		return möglicheOrte.get(nächstesElement);
	}

}
package de.dhbw.ase;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Starter {

	public static void main(String[] args) {
		Map<PersonId, List<Ort>> orte = leseOrte();
		Zielwahl zielwahl = new StatischeZielwahl(orte);
	}

	private static Map<PersonId, List<Ort>> leseOrte() {
		Map<PersonId, List<Ort>> orte = new HashMap<PersonId, List<Ort>>();
		orte.put(PersonId.createNextId(), Arrays.asList(new Ort(null, null, null)));
		return orte;
	}

}
